'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglifyjs = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');

sass.compiler = require('node-sass');

gulp.task('sass', function () {
    return gulp.src('./CSS/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./CSS'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./CSS/*.scss', gulp.series('sass'));
});

gulp.task('browser-sync', function () {
    var files = ['./*.html', './CSS/*.css', './img/*.{png, jpg, gif}', './js/*.js'];
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});
gulp.task('default', gulp.parallel('browser-sync', 'sass:watch'));

gulp.task('clean', function () {
    return del(['dist']);
});

gulp.task('copyfonts', function (done) {
    gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eot,svg,otf}')
        .pipe(gulp.dest('./dist/fonts/'));
    done();
});

gulp.task('imagemin', function () {
    return gulp.src('./img/*.{png,jpg,jpeg,gif}')
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
        .pipe(gulp.dest('dist/img'));
});

gulp.task('usemin', function () {
    return gulp.src('./*.html')
        .pipe(flatmap(function (stream, file) {
            return stream
                .pipe(usemin({
                    css: [rev()],
                    html: [function () { return htmlmin({ collapseWhitespace: true }) }],
                    js: [uglifyjs(), rev()],
                    inlinejs: [uglifyjs()],
                    inlinecss: [cleanCss(), 'concat']
                }));
        }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('build', gulp.series('clean',
    gulp.parallel('copyfonts', 'imagemin', 'usemin')
));