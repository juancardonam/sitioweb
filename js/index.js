$(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
    $('.carousel').carousel({
        interval: 2000
    })
    $('#contacto').on('show.bs.modal',
        function (e) {
            console.log('El modal se abrio')
            $('#contactoBtn').prop('disabled', true)
            $('#contactoBtn').css('background-color', "gray")
        })
    $('#contacto').on('shown.bs.modal',
        function (e) {
            console.log('El modal se termino de  abrir')
        })
    $('#contacto').on('hide.bs.modal',
        function (e) {
            console.log('El modal se empezo a cerrar')
        })
    $('#contacto').on('hidden.bs.modal',
        function (e) {
            console.log('El modal se termino de cerrar')
            $('#contactoBtn').prop('disabled', false)
            $('#contactoBtn').css('background-color', "")

        })
});